'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">trustit documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-AppModule-e9a1be04f6a813fbf01f6b2aabbfca77a123a6d36ad102165bdf309a9c147528ff543687492d7402f869d280c802564312bb9b39df9570f63ec675fa6b7a68bc"' : 'data-target="#xs-components-links-module-AppModule-e9a1be04f6a813fbf01f6b2aabbfca77a123a6d36ad102165bdf309a9c147528ff543687492d7402f869d280c802564312bb9b39df9570f63ec675fa6b7a68bc"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-e9a1be04f6a813fbf01f6b2aabbfca77a123a6d36ad102165bdf309a9c147528ff543687492d7402f869d280c802564312bb9b39df9570f63ec675fa6b7a68bc"' :
                                            'id="xs-components-links-module-AppModule-e9a1be04f6a813fbf01f6b2aabbfca77a123a6d36ad102165bdf309a9c147528ff543687492d7402f869d280c802564312bb9b39df9570f63ec675fa6b7a68bc"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/NavbarComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >NavbarComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SettingsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SettingsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ContactsModule.html" data-type="entity-link" >ContactsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-ContactsModule-4250929c718d2baf3ab48eb833cc238520a6e5c6710a054b51f3ad82d1a35e6fd5eb618f733ebb2bd6686554408b2563c547f9834966ec4eafc0f49ee0197124"' : 'data-target="#xs-components-links-module-ContactsModule-4250929c718d2baf3ab48eb833cc238520a6e5c6710a054b51f3ad82d1a35e6fd5eb618f733ebb2bd6686554408b2563c547f9834966ec4eafc0f49ee0197124"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ContactsModule-4250929c718d2baf3ab48eb833cc238520a6e5c6710a054b51f3ad82d1a35e6fd5eb618f733ebb2bd6686554408b2563c547f9834966ec4eafc0f49ee0197124"' :
                                            'id="xs-components-links-module-ContactsModule-4250929c718d2baf3ab48eb833cc238520a6e5c6710a054b51f3ad82d1a35e6fd5eb618f733ebb2bd6686554408b2563c547f9834966ec4eafc0f49ee0197124"' }>
                                            <li class="link">
                                                <a href="components/ContactsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContactsComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ContactsMapComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ContactsMapComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginModule.html" data-type="entity-link" >LoginModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-LoginModule-ab4bfd2167d4bb461dd2bd7a8f784f288277efe37326f1b8f25a4f6cd84f82436e3caa77dd7a1e685b9a4634edb9b3893374b77cbf6a5935d252fbb25913cc5f"' : 'data-target="#xs-components-links-module-LoginModule-ab4bfd2167d4bb461dd2bd7a8f784f288277efe37326f1b8f25a4f6cd84f82436e3caa77dd7a1e685b9a4634edb9b3893374b77cbf6a5935d252fbb25913cc5f"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-LoginModule-ab4bfd2167d4bb461dd2bd7a8f784f288277efe37326f1b8f25a4f6cd84f82436e3caa77dd7a1e685b9a4634edb9b3893374b77cbf6a5935d252fbb25913cc5f"' :
                                            'id="xs-components-links-module-LoginModule-ab4bfd2167d4bb461dd2bd7a8f784f288277efe37326f1b8f25a4f6cd84f82436e3caa77dd7a1e685b9a4634edb9b3893374b77cbf6a5935d252fbb25913cc5f"' }>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LostPassComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LostPassComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegistrationComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegistrationComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/SignInComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SignInComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/LoginRoutingModule.html" data-type="entity-link" >LoginRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/SharedModule.html" data-type="entity-link" >SharedModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' : 'data-target="#xs-components-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' :
                                            'id="xs-components-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                            <li class="link">
                                                <a href="components/AccordionComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/AccordionGroupComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AccordionGroupComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/CardComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >CardComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/ColComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >ColComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/GmapComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GmapComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RowComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RowComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/WeatherComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >WeatherComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#directives-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' : 'data-target="#xs-directives-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' :
                                        'id="xs-directives-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                        <li class="link">
                                            <a href="directives/AlertDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AlertDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/EmailValidatorDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EmailValidatorDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/FormatInputDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >FormatInputDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/IfLoggedDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >IfLoggedDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/MarginDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >MarginDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/PadDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PadDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/StopPropagationDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StopPropagationDirective</a>
                                        </li>
                                        <li class="link">
                                            <a href="directives/UrlDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UrlDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#pipes-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' : 'data-target="#xs-pipes-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                            <span class="icon ion-md-add"></span>
                                            <span>Pipes</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="pipes-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' :
                                            'id="xs-pipes-links-module-SharedModule-35779f7601f909acb3f62501a81c6ae47a52ad94148dc9b87d79b17a0f383c5f2a40f774f0c06c46a1625c66e914c7d706616da7dc6db1ecd92e07534a53ae68"' }>
                                            <li class="link">
                                                <a href="pipes/SquarePipe.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >SquarePipe</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit1demoModule.html" data-type="entity-link" >Uikit1demoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Uikit1demoModule-2051663f764ca7ffec5837c901af648898c3577811430e1d78536598af4f4cae8808429d53b108ebab7b276ef585e4f1315875b5612cb661d4c07b48debcd3ff"' : 'data-target="#xs-components-links-module-Uikit1demoModule-2051663f764ca7ffec5837c901af648898c3577811430e1d78536598af4f4cae8808429d53b108ebab7b276ef585e4f1315875b5612cb661d4c07b48debcd3ff"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Uikit1demoModule-2051663f764ca7ffec5837c901af648898c3577811430e1d78536598af4f4cae8808429d53b108ebab7b276ef585e4f1315875b5612cb661d4c07b48debcd3ff"' :
                                            'id="xs-components-links-module-Uikit1demoModule-2051663f764ca7ffec5837c901af648898c3577811430e1d78536598af4f4cae8808429d53b108ebab7b276ef585e4f1315875b5612cb661d4c07b48debcd3ff"' }>
                                            <li class="link">
                                                <a href="components/Uikit1demoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Uikit1demoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit1demoRoutingModule.html" data-type="entity-link" >Uikit1demoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit2demoModule.html" data-type="entity-link" >Uikit2demoModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-Uikit2demoModule-756ddb5e5f2ddd7c84a936b1fe789276a9a908a494edca099c7ca361f7cb34cb0570dc896ff3076dbe79b5ff63d703cd65eb99b40d39b91d35a12f8fccd4ecc3"' : 'data-target="#xs-components-links-module-Uikit2demoModule-756ddb5e5f2ddd7c84a936b1fe789276a9a908a494edca099c7ca361f7cb34cb0570dc896ff3076dbe79b5ff63d703cd65eb99b40d39b91d35a12f8fccd4ecc3"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-Uikit2demoModule-756ddb5e5f2ddd7c84a936b1fe789276a9a908a494edca099c7ca361f7cb34cb0570dc896ff3076dbe79b5ff63d703cd65eb99b40d39b91d35a12f8fccd4ecc3"' :
                                            'id="xs-components-links-module-Uikit2demoModule-756ddb5e5f2ddd7c84a936b1fe789276a9a908a494edca099c7ca361f7cb34cb0570dc896ff3076dbe79b5ff63d703cd65eb99b40d39b91d35a12f8fccd4ecc3"' }>
                                            <li class="link">
                                                <a href="components/Uikit2demoComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >Uikit2demoComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/Uikit2demoRoutingModule.html" data-type="entity-link" >Uikit2demoRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UsersDetailsModule.html" data-type="entity-link" >UsersDetailsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersDetailsModule-5c6ff55e4ca3a1352c4cf4603998cb03aa1cf709a76efd52007f6577c6eff96a80275f8dca3e29da8f2ba76d2d10d1b58439006c744c7f9a4bb5e6baf1bd26a2"' : 'data-target="#xs-components-links-module-UsersDetailsModule-5c6ff55e4ca3a1352c4cf4603998cb03aa1cf709a76efd52007f6577c6eff96a80275f8dca3e29da8f2ba76d2d10d1b58439006c744c7f9a4bb5e6baf1bd26a2"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersDetailsModule-5c6ff55e4ca3a1352c4cf4603998cb03aa1cf709a76efd52007f6577c6eff96a80275f8dca3e29da8f2ba76d2d10d1b58439006c744c7f9a4bb5e6baf1bd26a2"' :
                                            'id="xs-components-links-module-UsersDetailsModule-5c6ff55e4ca3a1352c4cf4603998cb03aa1cf709a76efd52007f6577c6eff96a80275f8dca3e29da8f2ba76d2d10d1b58439006c744c7f9a4bb5e6baf1bd26a2"' }>
                                            <li class="link">
                                                <a href="components/UsersDetailsComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersDetailsComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersDetailsRoutingModule.html" data-type="entity-link" >UsersDetailsRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#components-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' : 'data-target="#xs-components-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' :
                                            'id="xs-components-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' }>
                                            <li class="link">
                                                <a href="components/UsersComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersFormComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersFormComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersListComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersListComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/UsersListItemComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersListItemComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' : 'data-target="#xs-injectables-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' :
                                        'id="xs-injectables-links-module-UsersModule-7c0179a12ffba0635ad9efddb6ea2561efc9d90406884ce0ed4568b219456237002b78e86cf6f922095ef903fb36572a85865da1be9b291af7a1867a743d2cc1"' }>
                                        <li class="link">
                                            <a href="injectables/UsersStoreService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersStoreService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersRoutingModule.html" data-type="entity-link" >UsersRoutingModule</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/AuthService.html" data-type="entity-link" >AuthService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/ThemeService.html" data-type="entity-link" >ThemeService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UsersService.html" data-type="entity-link" >UsersService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interceptors-links"' :
                            'data-target="#xs-interceptors-links"' }>
                            <span class="icon ion-ios-swap"></span>
                            <span>Interceptors</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="interceptors-links"' : 'id="xs-interceptors-links"' }>
                            <li class="link">
                                <a href="interceptors/AuthInterceptor.html" data-type="entity-link" >AuthInterceptor</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/AuthGuard.html" data-type="entity-link" >AuthGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Address.html" data-type="entity-link" >Address</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Auth.html" data-type="entity-link" >Auth</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Company.html" data-type="entity-link" >Company</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Geo.html" data-type="entity-link" >Geo</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SettingsComponent } from './features/settings/settings.component';
import { AuthGuard } from './core/auth/auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: 'contacts', canActivate: [AuthGuard], loadChildren: () => import('./features/contacts/contacts.module').then(m => m.ContactsModule)},
      { path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)},
      { path: 'users/:userId', loadChildren: () => import('./features/users-details/users-details.module').then(m => m.UsersDetailsModule) },
      { path: 'settings', component: SettingsComponent},
      { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
      { path: 'uikit1', loadChildren: () => import('./features/uikit1demo/uikit1demo.module').then(m => m.Uikit1demoModule) },
      { path: 'uikit2', loadChildren: () => import('./features/uikit2demo/uikit2demo.module').then(m => m.Uikit2demoModule) },
      { path: 'uikit3', loadChildren: () => import('./features/uikit3demo/uikit3demo.module').then(m => m.Uikit3demoModule) },
      { path: 'di', loadChildren: () => import('./features/di/di.module').then(m => m.DiModule) },
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SettingsComponent } from './features/settings/settings.component';
import { NavbarComponent } from './core/layout/navbar.component';
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';
import { LogService } from './core/services/log.service';
import { environment } from '../environments/environment';
import { FakeLogService } from './core/services/fake-log.service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SettingsComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: LogService,
      useFactory: (http: HttpClient) => {
        return environment.production ? new LogService(http, 123) : new FakeLogService()
      },
      deps: [HttpClient]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

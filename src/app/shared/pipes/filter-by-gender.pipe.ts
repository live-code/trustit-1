import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(users: User[], gender: 'M' | 'F' | ''): User[] {
    if (gender === '') return users;
    return users.filter(u => u.gender === gender);
  }

}

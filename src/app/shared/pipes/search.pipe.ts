import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../../model/user';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(users: User[], text: string): User[] {
    return users.filter(u => {
      return u.name.toLowerCase().includes(text.toLowerCase())
    })
  }

}

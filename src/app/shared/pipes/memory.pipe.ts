import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'memory'
})
export class MemoryPipe implements PipeTransform {

  transform(
    value: number,
    formatType: 'byte' | 'mb' = 'mb',
    anotherParam?: any
  ): number {
    console.log(anotherParam)
    switch (formatType) {
      case 'byte': return value * 1000;
      case 'mb': return value * 1000000;
    }
  }

}

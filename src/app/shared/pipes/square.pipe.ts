import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'tsSquare'
})
export class SquarePipe implements PipeTransform {
  transform(
    value: number,
    multiplier: number,
    prefix: string
  ): string {
    console.log('render')
    return prefix + (value * multiplier)
  }

}

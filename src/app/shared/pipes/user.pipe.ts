import { Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable, share } from 'rxjs';
import { User } from '../../model/user';

@Pipe({
  name: 'user'
})
export class UserPipe implements PipeTransform {

  constructor(private http: HttpClient) {}

  transform(id: number): Observable<User> {
    return this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
      .pipe(
        share()
      )
      /*.pipe(
        map(user => `${user.name} (${user.email})`)
      )*/
  }

}

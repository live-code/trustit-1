import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[tsFormatInput]'
})
export class FormatInputDirective {

  @HostListener('input', ['$event'])
  changeHandler(event: KeyboardEvent) {
    const input = (event.currentTarget as HTMLInputElement).value;
    (event.currentTarget as HTMLInputElement).value = input.toUpperCase()
  }

}

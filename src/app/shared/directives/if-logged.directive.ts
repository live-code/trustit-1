import { Directive, HostBinding, Input } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';

@Directive({
  selector: '[tsIfLogged]'
})
export class IfLoggedDirective {
  @Input() tsIfLogged: 'admin' | 'moderator' | undefined;

  @HostBinding() get hidden() {
    return !(this.authService.role === this.tsIfLogged)
  }
  constructor(private authService: AuthService) {

  }

}

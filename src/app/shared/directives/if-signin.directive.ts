import { Component, ComponentRef, Directive, HostBinding, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { CardComponent } from '../components/card.component';
import { AuthService } from '../../core/auth/auth.service';
import { map } from 'rxjs';

@Directive({
  selector: '[tsIfSignin]'
})
export class IfSigninDirective {
  constructor(
    private tpl: TemplateRef<any>,
    private view: ViewContainerRef,
    private auth: AuthService
  ) {
    this.auth.data$
      .pipe(
        map(val => !!val)
      )
      .subscribe(res => {
        if (res) {
          this.view.createEmbeddedView(this.tpl)
        } else {
          this.view.clear()
        }
      })
    /*this.obs$.subscribe(value => {

    })*/
  }

}

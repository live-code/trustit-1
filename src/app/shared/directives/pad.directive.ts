import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[tsPad]'
})
export class PadDirective {
  @HostBinding() get className() {
    return this.double ? 'p-4' : 'p-2';
  }
  @HostBinding('style.fontSize') abc = '50px'
  @Input() double: boolean = false;

}

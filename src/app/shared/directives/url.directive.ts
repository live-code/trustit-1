import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[tsUrl]'
})
export class UrlDirective {
  @Input() tsUrl: string | undefined;

  @HostListener('click')
  clickMe() {
    window.open(this.tsUrl)
  }
}

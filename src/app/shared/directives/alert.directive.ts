import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[tsAlert]'
})
export class AlertDirective {
  @Input() tsAlert: 'danger' | 'info' | 'success' | undefined;
  @HostBinding() get class() {
    const cls = this.tsAlert || 'info';
    return `alert alert-${cls}`
  }
}

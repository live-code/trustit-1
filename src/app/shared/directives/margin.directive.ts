import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[tsMargin]'
})
export class MarginDirective {
  @Input() set tsMargin(val: string | number) {
    // this.el.nativeElement.style.margin = `${val}px`;
    this.renderer.addClass(this.el.nativeElement, 'ciao')
    this.renderer.setStyle(this.el.nativeElement, 'margin', `${val}px`)
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {

  }

  /*
  @Input() set tsMargin: number | string;
  ngOnChanges(changes: SimpleChanges) {
    if (changes['tsMargin']) {
      this.renderer.addClass(this.el.nativeElement, 'ciao')
      this.renderer.setStyle(this.el.nativeElement, 'margin', `${val}px`)
    }
  }
  */
}

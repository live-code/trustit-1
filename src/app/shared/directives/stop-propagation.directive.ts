import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[tsStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  clickMe(event: MouseEvent) {
    console.log((event.currentTarget as HTMLElement).style)
    event.stopPropagation();

  }

}

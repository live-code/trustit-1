import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

export const EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

@Directive({
  selector: '[tsEmailValidator]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true}
  ]
})
export class EmailValidatorDirective implements Validator {

  constructor() {
    console.log('EmailValidatorDirective ')
  }

  validate(control: AbstractControl): ValidationErrors | null {
    if (!control.value?.length) {
      return {
        required: true
      }
    }

    if (control.value?.length < 3) {
      return {
        troppoCorto: true
      }
    }
    if (control.value?.includes('pippo')) {
      return {
        pippo: true,
      }
    }
    if (!control.value?.match(EMAIL_REGEX)) {
      return {
        email: true,
      }
    }
    return null;
  }

}

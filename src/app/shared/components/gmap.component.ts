import { Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { google } from 'google-maps';
// declare var L: any;

@Component({
  selector: 'ts-gmap',
  template: `
    <div #host style="width: 300px; height: 200px"></div>
  `,
})
export class GmapComponent implements OnInit {
  @ViewChild('host', { static: true }) element!: ElementRef<HTMLDivElement>
  @Input() coords!: any;
  @Input() zoom: number = 10;

  map!: google.maps.Map;

  initMap(): void {
    this.map = new google.maps.Map(this.element.nativeElement as HTMLElement, {
      center: this.coords,
      zoom: this.zoom,
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['coords']) {
      if (changes['coords'].firstChange) {
        this.initMap();
      } else {
        this.map.panTo(new google.maps.LatLng(this.coords))
      }
    }

    if (changes['zoom']) {
      this.map.setZoom(this.zoom)
    }
  }

  ngOnInit(): void {
    console.log('ngOnInit', this.element)
  }

  ngAfterViewInit(): void {
    console.log('ngAfterViewInit', this.element)
    this.initMap()
  }

}

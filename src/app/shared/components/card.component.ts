import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

/**
 * Questo è un card component
 */
@Component({
  selector: 'ts-card',
  template: `
    <div class="card" [ngClass]="{'mb-3': marginBottom}">
      <div 
        class="card-header"
        [ngClass]="headerCls"
        (click)="toggleHandler()"
      >
        {{title}}
        
        <div class="pull-right">
          <i *ngIf="icon"
             (click)="iconClick.emit()"
             [class]="icon"></i>
        </div>
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  /**
   * Questo è il titolo
   */
  @Input() title: number | string = 'widget';
  @Input() icon: string = '';
  @Input() headerCls: string = '';
  @Input() marginBottom: boolean = false;
  @Input() opened!: boolean ;
  /**
   * bla bla
   */
  @Output() iconClick = new EventEmitter();
  @Output() openedChange = new EventEmitter();

  toggleHandler() {
    this.opened = !this.opened;
    this.openedChange.emit(this.opened)
  }
}

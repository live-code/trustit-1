import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ts-weather',
  template: `
    <p>
      weather works! {{city}}
    </p>
    
    <pre *ngIf="data">{{data?.main.temp}}°</pre>
  `,
  styles: [
  ]
})
export class WeatherComponent implements OnChanges {
  @Input() city: string = '';
  @Input() unit: 'metric' | 'imperial' = 'metric'
  data: any;

  constructor(private http: HttpClient) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.city) {
      this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.data = res;
        })
    }
/*
    if (changes['city']) {
      console.log('cambiato city')
    }
    if (changes['unit']) {
      console.log('cambiato unit')
    }*/
  }


}

import { AfterContentInit, Component, ContentChildren, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CardComponent } from './card.component';
import { AccordionGroupComponent } from './accordion-group.component';

@Component({
  selector: 'ts-accordion',
  template: `
    <div style="border: 10px solid black; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements AfterContentInit {
  @Input() openMultiple = false;
  @ContentChildren(AccordionGroupComponent) groups!: QueryList<AccordionGroupComponent>;

  ngAfterContentInit(): void {
    const firstGroup = this.groups.get(0);
    if (firstGroup) {
      firstGroup.opened = true;
    }
    if (this.openMultiple) {
      for(let g of this.groups.toArray()) {
        g.headerClick.subscribe(() => {
          g.opened = !g.opened;
        })
      }
    } else {
      for(let g of this.groups.toArray()) {
        g.headerClick.subscribe(() => {
          this.closeAll();
          g.opened = true;
        })
      }
    }

  }

  closeAll() {
    for(let g of this.groups.toArray()) {
      g.opened = false;
    }
  }

}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ts-accordion-group',
  template: `
    <div class="card">
      <div class="card-header" (click)="headerClick.emit()">
        {{title}}
      </div>
      <div class="card-body" *ngIf="opened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [
  ]
})
export class AccordionGroupComponent implements OnInit {
  @Input() title: string = '';
  @Input() opened: boolean = false;
  @Output() headerClick = new EventEmitter()

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ts-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
})
export class RowComponent {
  @Input() mq: 'sm' | 'md' | 'lg' = 'sm'
}

import { Component, OnInit, HostBinding, Input, Optional } from '@angular/core';
import { RowComponent } from './row.component';

@Component({
  selector: 'ts-col',
  template: `
    <ng-content></ng-content>
  `,
})
export class ColComponent {
  @HostBinding() get class() {
    return `col-${this.parent ? this.parent.mq : 'sm'}-6`
  }

  constructor(@Optional() private parent: RowComponent) {
  }
}

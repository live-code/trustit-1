import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { EmailValidatorDirective } from './forms/email-validator.directive';
import { SquarePipe } from './pipes/square.pipe';
import { WeatherComponent } from './components/weather.component';
import { GmapComponent } from './components/gmap.component';
import { AccordionGroupComponent } from './components/accordion-group.component';
import { AccordionComponent } from './components/accordion.component';
import { RowComponent } from './components/row.component';
import { ColComponent } from './components/col.component';
import { AlertDirective } from './directives/alert.directive';
import { PadDirective } from './directives/pad.directive';
import { MarginDirective } from './directives/margin.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { FormatInputDirective } from './directives/format-input.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import { MemoryPipe } from './pipes/memory.pipe';
import { UserPipe } from './pipes/user.pipe';
import { SearchPipe } from './pipes/search.pipe';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { SortPipe } from './pipes/sort.pipe';



@NgModule({
  declarations: [
    CardComponent,
    EmailValidatorDirective,
    SquarePipe,
    WeatherComponent,
    GmapComponent,
    AccordionGroupComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    AlertDirective,
    PadDirective,
    MarginDirective,
    IfLoggedDirective,
    UrlDirective,
    StopPropagationDirective,
    FormatInputDirective,
    IfSigninDirective,
    MemoryPipe,
    UserPipe,
    SearchPipe,
    FilterByGenderPipe,
    SortPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    EmailValidatorDirective,
    SquarePipe,
    WeatherComponent,
    GmapComponent,
    AccordionGroupComponent,
    AccordionComponent,
    RowComponent,
    ColComponent,
    AlertDirective,
    PadDirective,
    MarginDirective,
    IfLoggedDirective,
    UrlDirective,
    StopPropagationDirective,
    FormatInputDirective,
    IfSigninDirective,
    MemoryPipe,
    UserPipe,
    SearchPipe,
    FilterByGenderPipe,
    SortPipe
  ]
})
export class SharedModule { }

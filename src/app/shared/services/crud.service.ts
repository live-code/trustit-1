import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class CrudService<T> {
  counter = 0;
  endpoint!: string ;
  data: T[] = []
  inc() {
    this.counter ++;
  }

  getAll(): void {
    this.http.get<T[]>(this.endpoint)
      .subscribe(res => this.data = res)
  }

  constructor(private http: HttpClient) {
  }
}

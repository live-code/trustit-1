import { Component } from '@angular/core';

@Component({
  selector: 'ts-root',
  template: `
   
    <ts-navbar></ts-navbar>
    <hr>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {

}

import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let clonedReq = req;
    if (this.authService.isLogged() && this.authService.token && !req.url.includes('weather') ) {
      clonedReq = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authService.token}`
        }
      })
    }
    return next.handle(clonedReq)
      .pipe(
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
           switch (err.status) {
             case 401:
               this.router.navigateByUrl('/login')
               break;
             default:
             case 404:
               // this.router.navigateByUrl('/login')
               break;
           }
          }
          return throwError(err)
        })
      )
  }

  constructor(private authService: AuthService, private router: Router) {
  }
}

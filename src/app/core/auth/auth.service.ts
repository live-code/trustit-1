import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Auth } from '../../model/auth';
import { Router } from '@angular/router';
import { BehaviorSubject, delay, lastValueFrom, map, Observable, of, share } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: Auth | null = null;
  data$ = new BehaviorSubject<Auth | null>(null);

  constructor(private http: HttpClient, private router: Router) {
    const auth: string | null = localStorage.getItem('auth')
    this.data =  auth ? (JSON.parse(auth) as Auth) : null;
  }

   login(data: { username: string, password: string }): Observable<Auth> {
      const params = new HttpParams()
        .set('username', data.username)
        .set('password', data.password);

      const req$ = this.http.get<Auth>('http://localhost:3000/login', { params })
        .pipe(
          delay(2000),
          share()
        )

     req$.subscribe(
          res => {
            console.log('1')
            localStorage.setItem('auth', JSON.stringify(res))
            this.data = res;
            this.data$.next(res)
          },
        )

     return req$;
  }


   login1(data: { username: string, password: string }): Promise<Auth> {
      const params = new HttpParams()
        .set('username', data.username)
        .set('password', data.password);

      const req$ = this.http.get<Auth>('http://localhost:3000/login', { params })
        .pipe(
          delay(2000)
        )
      req$.subscribe(
          res => {
            console.log('1')
            localStorage.setItem('auth', JSON.stringify(res))
            this.data = res;
          },
        )
      return lastValueFrom(req$)
  }


  login2(data: { username: string, password: string }): Promise<Auth> {
    return new Promise<Auth>((resolve, reject) => {
      const params = new HttpParams()
        .set('username', data.username)
        .set('password', data.password);

      this.http.get<Auth>('http://localhost:3000/login', { params })
        .subscribe(
          res => {
            localStorage.setItem('auth', JSON.stringify(res))
            this.data = res;
            resolve(res);
          },
          err => {
            reject('ahia!!')
          }
        )
    })
  }


  logout() {
    this.data = null;
    this.data$.next(null)
    localStorage.removeItem('auth')
  }


  isLogged(): boolean {
    // return !!localStorage.getItem('auth')
    return !!this.data
  }


  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        map(val => !!val)
      )
  }

  get role(): string | null {
    return this.data ? this.data?.role : null
  }


  get token(): string | undefined {
    return this.data?.token
  }
}

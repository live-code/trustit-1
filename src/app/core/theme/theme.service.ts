import { Injectable } from '@angular/core';

type Theme = 'dark' | 'light';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private _value: Theme = 'dark';

  constructor() {
    const getTheme: Theme = localStorage.getItem('theme') as Theme;
    if (getTheme) {
      this._value = getTheme;
    }
  }

  set theme(val: Theme) {
    this._value = val;
    localStorage.setItem('theme', val)
  }

  get theme(): Theme {
    return this._value
  }

}

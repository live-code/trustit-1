import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../theme/theme.service';
import { UsersService } from '../../features/users/services/users.service';
import { UsersStoreService } from '../../features/users/services/users-store.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ts-navbar',
  template: `
    <div 
      class="p-2" 
      [ngClass]="{
        'bg-info': themeService.theme === 'light',
        'bg-dark': themeService.theme === 'dark'
      }"
    >
      <button routerLink="contacts" *ngIf="authService.isLogged()">contacts</button>
      <button routerLink="users" tsIfLogged="moderator">users</button>
      <button routerLink="settings" *tsIfSignin>settings</button>
      
      <button routerLink="uikit1">uikit 1</button>
      <button routerLink="uikit2">uikit 2</button>
      <button routerLink="uikit3">uikit 3</button>
      <button routerLink="login">login</button>
      <button (click)="logoutHandler()">logout</button>

    </div>
     
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {


  constructor(
    public themeService: ThemeService,
    public authService: AuthService,
    private router: Router
  ) {
    console.log(themeService.theme)
  }

  ngOnInit(): void {
  }

  logoutHandler() {
    this.authService.logout()
    this.router.navigateByUrl('login')
  }
}

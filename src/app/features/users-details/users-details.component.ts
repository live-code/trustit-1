import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { catchError, delay, distinct, distinctUntilChanged, filter, map, of, switchMap, tap } from 'rxjs';

@Component({
  selector: 'ts-users-details',
  template: `
    <div *ngIf="data$ | async as data">
      <pre>{{(data)?.name}}</pre>
      <pre>{{(data)?.email}}</pre>
      <button [routerLink]="'../' + (data)?.nextId">Next User</button>
    </div>
    <hr>
    <button routerLink="../">back to list</button>
    <button routerLink="/users">back to list</button>
  `,
})
export class UsersDetailsComponent {
  data$ = this.activatedRoute.params
    .pipe(
      delay(400),
      distinctUntilChanged(),
      // filter(res => res['userId'] < 4),
      switchMap(
        res => this.http.get<User>(`http://localhost:3000/users/${res['userId']}`)
          .pipe(
            catchError(
              err =>  of(null)
                .pipe(
                  tap(() => this.router.navigateByUrl('/users/1'))
                )
            ),
          )
      ),
    )

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private router: Router
  ) {
  }

}

import { NgModule } from '@angular/core';
import { ContactsMapComponent } from './components/contacts-map/contacts-map.component';
import { ContactsComponent } from './contacts.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ContactsComponent,
    ContactsMapComponent,
  ],
  imports: [
    RouterModule.forChild([
      { path: '', component: ContactsComponent}
    ])
  ]
})
export class ContactsModule {

}

import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme/theme.service';
import { HttpClient } from '@angular/common/http';
import { catchError, of } from 'rxjs';

@Component({
  selector: 'ts-contacts',
  template: `
    <p>
      contacts works!
    </p>
    
    <ts-contacts-map></ts-contacts-map>
  `,
  styles: [
  ]
})
export class ContactsComponent implements OnInit {

  // ------------xo------->
  constructor(private themeService: ThemeService, private http: HttpClient) {
    this.http.get('https://jsonplaceholder.typicode.com/users')
      .subscribe(
        res => console.log('success', res),
        err => console.log('err', err),
      )
  }

  ngOnInit(): void {
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DiRoutingModule } from './di-routing.module';
import { DiComponent } from './di.component';
import { ListComponent } from './components/list.component';
import { SharedModule } from '../../shared/shared.module';
import { CrudService } from '../../shared/services/crud.service';
import { LogService } from '../../core/services/log.service';
import { FakeLogService } from '../../core/services/fake-log.service';


@NgModule({
  declarations: [
    DiComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    DiRoutingModule,
    SharedModule
  ],
  providers: [
    { provide: LogService, useClass: FakeLogService}
  ]
})
export class DiModule { }

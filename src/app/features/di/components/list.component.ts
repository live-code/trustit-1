import { Component, Input, OnInit } from '@angular/core';
import { CrudService } from '../../../shared/services/crud.service';
import { User } from '../../../model/user';

@Component({
  selector: 'ts-list',
  template: `
    <p>
      list works! {{endpoint}}
    </p>
    
    <pre>{{crud.data | json}}</pre>
    <ts-gmap></ts-gmap>
  `,
  providers: [
    CrudService
  ]
})
export class ListComponent implements OnInit {
  @Input() endpoint: string | undefined;

  constructor(public crud: CrudService<any>) {}

  ngOnInit(): void {
    if (this.endpoint) {
      this.crud.endpoint = this.endpoint;
      this.crud.getAll()
    }
  }

}

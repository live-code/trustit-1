import { Component, OnInit } from '@angular/core';
import { LogService } from '../../core/services/log.service';

@Component({
  selector: 'ts-di',
  template: `
    
    <ts-row>
      <ts-col>
        <ts-list
          endpoint="https://jsonplaceholder.typicode.com/users"></ts-list>
      </ts-col>
      <ts-col>
        <ts-list
          endpoint="https://jsonplaceholder.typicode.com/posts"></ts-list>
      </ts-col>
    </ts-row>
  `,
  styles: [
  ]
})
export class DiComponent implements OnInit {

  constructor(private logService: LogService) { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { CrudService } from '../../shared/services/crud.service';

@Component({
  selector: 'ts-uikit2demo',
  template: `
    
    <h1>if SignIn</h1>
    
    <h1>url</h1>
    <button tsUrl="http://www.fabiobiondi.io">Visit It</button>
    
    <div style="padding: 10px; background-color: red" (click)="parentHandler()">
      parent
      <div  style="margin: 10px; padding: 10px; background-color: cyan"  (click)="childHandler()" tsStopPropagation>child</div>
    </div>
    
    <input type="text" tsFormatInput>
    
    <h1>Margin</h1>
    <div tsMargin="2">margin</div>
    <div [tsMargin]="marginValue">margin</div>
    <button (click)="marginValue = marginValue + 5">+</button>
    
    <hr>
    <h1>Padding</h1>
    <div tsPad >b</div>
    <div tsPad [double]="true">b</div>

    <hr>
    <h1>Alert</h1>
    <div [tsAlert]>alert</div>
    <div tsAlert="danger">alert</div>
    <div [tsAlert]="value">alert</div>
    
    <button (click)="value = 'info'">info</button>
    <button (click)="value = 'danger'">danger</button>
    
    <hr>
  `,
})
export class Uikit2demoComponent {
  value: 'danger' | 'info' | 'success' = 'success'
  marginValue = 5;

  parentHandler() {
    window.alert('parent')

  }
  childHandler() {
    window.alert('child')
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit2demoRoutingModule } from './uikit2demo-routing.module';
import { Uikit2demoComponent } from './uikit2demo.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CrudService } from '../../shared/services/crud.service';


@NgModule({
  declarations: [
    Uikit2demoComponent
  ],
  imports: [
    CommonModule,
    Uikit2demoRoutingModule,
    SharedModule,
    FormsModule
  ],
  providers: [
    CrudService
  ]
})
export class Uikit2demoModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit2demoComponent } from './uikit2demo.component';

const routes: Routes = [{ path: '', component: Uikit2demoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit2demoRoutingModule { }

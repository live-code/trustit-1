import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/theme/theme.service';

@Component({
  selector: 'ts-settings',
  template: `
    
    <div>Current theme is {{themeService.theme}}</div>
    
    <hr>
    <button (click)="themeService.theme = 'dark' ">Dark</button>
    <button (click)="themeService.theme = 'light' ">Light</button>
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {

  constructor(public themeService: ThemeService) {
    console.log(themeService.theme)
  }

  ngOnInit(): void {
  }

}

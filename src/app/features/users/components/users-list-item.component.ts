import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ts-users-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li
      class="list-group-item"
      [ngClass]="{'active': selected}"
      (click)="setActive.emit(user)"
    >
      {{user.name}}
      <div class="pull-right">
        <i class="fa fa-link" [routerLink]="'/users/' + user.id "></i>
        <i class="fa fa-arrow-circle-down"
           (click)="toggleHandler($event)"></i>
        <i class="fa fa-trash"
           (click)="deleteHandler(user.id, $event)"></i>
      </div>
      <div *ngIf="opened">
        {{user.email}}
        {{user.phone}}
      </div>
    </li>
  `,
})
export class UsersListItemComponent  {
  @Input() user!: User;
  @Input() selected: boolean = false;
  @Output() delete = new EventEmitter<number>();
  @Output() setActive = new EventEmitter<User>();
  opened = false;

  toggleHandler(event: MouseEvent) {
    event.stopPropagation();
    this.opened = !this.opened
  }

  deleteHandler(id: number, event: MouseEvent) {
    event.stopPropagation();
    this.delete.emit(id);
    //   this.userActions.deleteHandler(id);
  }

  render() {
    console.log('render list item')
  }
}

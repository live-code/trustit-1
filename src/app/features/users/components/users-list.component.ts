import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../../../model/user';

@Component({
  selector: 'ts-users-list',
  template: `

    <ts-users-list-item
      *ngFor="let user of items"
      [user]="user"
      [selected]="user.id === activeUser?.id"
      (delete)="delete.emit($event)"
      (setActive)="setActive.emit($event)"
    ></ts-users-list-item>
  `,
})
export class UsersListComponent {
  @Input() items: User[] = [];
  @Input() activeUser: User | null = null;
  @Output() delete = new EventEmitter<number>();
  @Output() setActive = new EventEmitter<User>();

}

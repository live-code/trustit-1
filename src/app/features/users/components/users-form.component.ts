import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UsersService } from '../services/users.service';
import { UsersStoreService } from '../services/users-store.service';
import { User } from '../../../model/user';

@Component({
  selector: 'ts-users-form',
  template: `
    <div *ngIf="f.invalid && f.dirty">Form invalid</div>
    
    <form #f="ngForm" (submit)="save.emit(f.value)">
      <input
        type="text" class="form-control" name="name"
        [ngModel]="activeUser?.name"
        #inputName="ngModel" placeholder="name"
        required minlength="3"
        [ngClass]="{
            'is-valid': inputName.valid, 
            'is-invalid': inputName.invalid && f.dirty
          }"

      >

      <div *ngIf="f.dirty && inputName.errors as err">
        <div *ngIf="err['required']">field required</div>
        <div *ngIf="err['minlength'] as minL">
          field too short. Missing {{minL.requiredLength - minL.actualLength}} chars
        </div>
      </div>

      <input
        type="text" class="form-control" name="email"
        [ngModel]="activeUser?.email"
        placeholder="email"
        #inputEmail="ngModel"
        tsEmailValidator
        [ngClass]="{
            'is-valid': inputEmail.valid, 
            'is-invalid': inputEmail.invalid && f.dirty
          }"
      >

      <div *ngIf="f.dirty && inputEmail.errors as err">
        <div *ngIf="err['required']">field required</div>
        <div *ngIf="err['email']">email invalid</div>
        <div *ngIf="err['pippo']">non puoi usare pippo</div>
        <div *ngIf="err['troppoCorto'] as minL">
          field too short
        </div>
      </div>

      <button type="submit" (click)="clear.emit()"
              *ngIf="activeUser?.id">CLEAR</button>
      <button type="submit" [disabled]="f.invalid">SAVE</button>
    </form>
  `,
})
export class UsersFormComponent {
  @Input() activeUser: User | null = null;
  @Output() save = new EventEmitter<Pick<User, "name" | "email">>()
  @Output() clear = new EventEmitter()
}

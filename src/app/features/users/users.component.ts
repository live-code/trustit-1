import { Component } from '@angular/core';
import { UsersService } from './services/users.service';
import { UsersStoreService } from './services/users-store.service';

@Component({
  selector: 'ts-users',
  template: `
    <ts-card title="form" [opened]="true">
      <ts-users-form
        [activeUser]="userStore.activeUser"
        (save)="userActions.save($event)"
        (clear)="userActions.clearActiveUser()"
      ></ts-users-form>
    </ts-card>

    <ts-card [title]="userStore.users.length + ' users'" [opened]="true">
      <ts-users-list
        [items]="userStore.users"
        [activeUser]="userStore.getActiveUser()"
        (delete)="userActions.deleteHandler($event)"
        (setActive)="userActions.selectHandler($event)"
      ></ts-users-list>
    </ts-card>
    
    <button routerLink="/contacts">goto contacts</button>
  `,

})
export class UsersComponent {

  constructor(
    public userActions: UsersService,
    public userStore: UsersStoreService
  ) {
    this.userActions.getAll()
  }
}

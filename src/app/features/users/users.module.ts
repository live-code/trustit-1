import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersComponent } from './users.component';
import { UsersListComponent } from './components/users-list.component';
import { UsersFormComponent } from './components/users-form.component';
import { UsersListItemComponent } from './components/users-list-item.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { UsersRoutingModule } from './users-routing.module';
import { UsersStoreService } from './services/users-store.service';
import { UsersService } from './services/users.service';


@NgModule({
  declarations: [
    UsersComponent,
    UsersListComponent,
    UsersFormComponent,
    UsersListItemComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    UsersRoutingModule
    // CommonModule
  ],
  providers: [
    UsersStoreService,
    // UsersService
    { provide: UsersService, useClass: UsersService }
  ],
})
export class UsersModule { }

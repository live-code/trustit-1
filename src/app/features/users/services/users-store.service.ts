import { Injectable } from '@angular/core';
import { User } from '../../../model/user';

@Injectable()
export class UsersStoreService {
  users: User[] = [];
  activeUser: User | null = null;

  init(users: User[]) {
    this.users = users;
  }

  add(user: User) {
    this.users = [...this.users, user];
    //this.users.push(user);
    this.activeUser = user;
  }

  delete(id: number) {
    this.users = this.users.filter(user =>  user.id !== id);
    if (this.activeUser?.id === id) {
      this.activeUser = null;
    }
  }

  update(userToUpdate: User) {
    this.users = this.users.map(u => {
      return u.id === this.activeUser?.id ? userToUpdate : u;
    })
  }

  setActive(user: User) {
    this.activeUser = user;
  }

  getActiveUser(): User | null{
    return this.activeUser;
  }

  clearActiveUser() {
    this.activeUser = null;
  }
}

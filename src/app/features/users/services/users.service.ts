import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { UsersStoreService } from './users-store.service';

@Injectable()
export class UsersService {


  getAll() {
    this.http.get<User[]>('http://localhost:3000/users')
      .subscribe( (res) => {
        this.store.init(res)
      });
  }

  save(user: Pick<User, 'name' | 'email'>) {
    if (this.store.getActiveUser()) {
      this.edit(user)
    } else {
      this.add(user)
    }
  }

  edit(user: Pick<User, 'name' | 'email'>) {
    this.http.put<User>(`http://localhost:3000/users/${this.store.getActiveUser()?.id}`, user)
      .subscribe(res => this.store.update(res))
  }

  add(user: Pick<User, 'name' | 'email'>) {
    this.http.post<User>(`http://localhost:3000/users/`, user)
      .subscribe(res => this.store.add(res))
  }

  deleteHandler(id: number) {
    this.http.delete(`http://localhost:3000/users/${id}`)
      .subscribe(() =>  this.store.delete(id))
  }

  selectHandler(user: User) {
    this.store.setActive(user)
  }

  clearActiveUser() {
    this.store.clearActiveUser()
  }

  constructor(
    private http: HttpClient,
    private store: UsersStoreService
  ) {
  }
  
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit1demoRoutingModule } from './uikit1demo-routing.module';
import { Uikit1demoComponent } from './uikit1demo.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    Uikit1demoComponent
  ],
  imports: [
    CommonModule,
    Uikit1demoRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ]
})
export class Uikit1demoModule { }

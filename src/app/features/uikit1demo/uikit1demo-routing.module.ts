import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit1demoComponent } from './uikit1demo.component';

const routes: Routes = [{ path: '', component: Uikit1demoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit1demoRoutingModule { }

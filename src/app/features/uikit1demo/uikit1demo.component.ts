import {
  Component, ComponentRef,
  ElementRef,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
  ViewContainerRef
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs';
import { CardComponent } from '../../shared/components/card.component';

@Component({
  selector: 'ts-uikit1demo',
  template: `
 
    <ts-row mq="lg">
      <ts-col>
        <input type="text">
      </ts-col>
      <ts-col>
        <button>click me</button>
      </ts-col>
    </ts-row>
    
    <ts-accordion [openMultiple]="false">
      <ts-accordion-group title="uno">
        bla bla
      </ts-accordion-group>
      
      <ts-accordion-group title="GMAP">
        <ts-gmap [coords]="coords" [zoom]="zoom"></ts-gmap>
        <button (click)="zoom = zoom + 1">+</button>
        <button (click)="zoom = zoom - 1">-</button>
        <button (click)="coords = { lat: 41, lng: 12}">Italy</button>
        <button (click)="coords = { lat: 55, lng: 22}">Lituania</button>
      </ts-accordion-group>

      <ts-accordion-group title="Weather">
        <input type="text" [formControl]="input">
        <ts-weather [city]="cityText" [unit]="unitMeteo"></ts-weather>
        <button (click)="unitMeteo = 'imperial'">Imperial</button>
        <button (click)="unitMeteo = 'metric'">metric</button>
      </ts-accordion-group>
    </ts-accordion>
    
    <hr>
    <!--<div *ngFor="let config of data">
      <div *loader="config"></div>
    </div>
   -->
  `,
})
export class Uikit1demoComponent  {
  @ViewChild('nodata') tpl!: TemplateRef<any>
  users = null;
  input = new FormControl()
  cityText: string = ''
  unitMeteo: 'imperial' | 'metric' = 'metric'
  coords = { lat: 55, lng: 11}
  zoom = 5;

  constructor(private view: ViewContainerRef) {
    setTimeout(() => {
      console.log(this.tpl)
      const compo: ComponentRef<CardComponent> = view.createComponent(CardComponent)
      compo.instance.title = 'ciccio'
      compo.instance.opened = true;

    }, 2000)

    this.input.valueChanges
      .pipe(
        debounceTime(1000)
      )
      .subscribe(text => this.cityText = text)
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ts-login',
  template: `
  
    <router-outlet></router-outlet>
    <hr>
    <button routerLink="signin">sign in</button>
    <button routerLink="registration">regi</button>
    <button routerLink="lostpass">lost</button>
    
    
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { debounceTime, interval, map, Observable, of, switchMap, tap, timer } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { User } from '../../../model/user';
import { AuthService } from '../../../core/auth/auth.service';
import { Router } from '@angular/router';

function customValidator(c: FormControl): ValidationErrors | null {
  if (c.value === 'pippo') {
    return {
      pippo: true
    }
  }
  return null;
}


@Component({
  selector: 'ts-sign-in',
  template: `

    <form [formGroup]="form" (submit)="login()">
      {{form.get('username')?.value.length | tsSquare: 2 : 'yo'}}
      <i class="fa fa-spinner" *ngIf="form.get('username')?.pending"></i>
                    
      <input 
        type="text" formControlName="username" 
        class="form-control"
        [ngClass]="{'is-invalid': form.get('username')?.invalid}"
      >
      <input 
        type="text" formControlName="password"
        class="form-control"
        [ngClass]="{'is-invalid': form.get('password')?.invalid}"
      >
      <button type="submit" [disabled]="form.invalid || form.pending" >Login</button>
    </form>
    

  `,
})
export class SignInComponent {
  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private authService: AuthService,
    private router: Router
  ) {
    this.form = fb.group({
      username: [
        '',
        [Validators.required, customValidator],
        (c: FormControl) => this.asyncValidator(c)
      ],
      password: ['', [Validators.required, Validators.minLength(3)]]
    })
  }

  login() {
    console.log(this.form.value);
    this.authService.login(this.form.value)
      .subscribe(res => {
        this.router.navigateByUrl('contacts')
      })
    /*
    this.authService.login2(this.form.value)
      .then(res => {
        console.log('2', res)
        this.router.navigateByUrl('contacts')
      })
     */

  }

  asyncValidator(c: FormControl): ValidationErrors | null {
    return timer(1000)
      .pipe(
        switchMap(() => {
          return this.http.get<User[]>('http://localhost:3000/users?q=' + c.value)
            .pipe(
              map(res => {
                return res.length ? null : {  usernameNotExists: true }
              }),
              // tap(res => console.log(res))
            )
        })
      )


  }

  calc(a: number) {
    return a * a;
  }
}

import { Component } from '@angular/core';
import { User } from '../../model/user';
import { HttpClient } from '@angular/common/http';
import { CrudService } from '../../shared/services/crud.service';

@Component({
  selector: 'ts-uikit3demo',
  template: `
    <input type="number" [(ngModel)]="value" placeholder="value">
    {{value | memory: 'byte'}}
    <hr>
    <div *ngIf="value | user | async as data">
      {{data.name}}
      {{data.email}}
    </div>

    <input type="text" [(ngModel)]="filter.text" placeholder="Search Name">
    <select [(ngModel)]="filter.gender">
      <option value="">select </option>
      <option>M</option>
      <option>F</option>    
    </select>
    <select [(ngModel)]="filter.order">
      <option>ASC</option>
      <option>DESC</option>
    </select>

    
    <li *ngFor="let user of users | search: filter.text 
                                  | filterByGender: filter.gender 
                                  | sort: filter.order">
      {{user.name}} {{user.id}}
    </li>
    <hr>
    <h1 (click)="crud.inc()">{{crud.counter}}</h1>

    <li *ngFor="let user of crud.data">
      {{user.name}}
    </li>
  `,
})
export class Uikit3demoComponent  {
  value = 1;
  filter: { text:string, gender: 'M' | 'F' | '', order: 'ASC'} = {
    text: '',
    gender: '',
    order: 'ASC'
  }
  users: User[] = []

  constructor( public crud: CrudService<User>) {
    this.crud.endpoint = 'https://jsonplaceholder.typicode.com/users';
    this.crud.getAll();
  }
}

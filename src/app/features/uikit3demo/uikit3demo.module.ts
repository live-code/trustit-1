import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Uikit3demoRoutingModule } from './uikit3demo-routing.module';
import { Uikit3demoComponent } from './uikit3demo.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { CrudService } from '../../shared/services/crud.service';


@NgModule({
  declarations: [
    Uikit3demoComponent
  ],
  imports: [
    CommonModule,
    Uikit3demoRoutingModule,
    FormsModule,
    SharedModule
  ],
  providers: [
    CrudService
  ]
})
export class Uikit3demoModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Uikit3demoComponent } from './uikit3demo.component';

const routes: Routes = [{ path: '', component: Uikit3demoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Uikit3demoRoutingModule { }
